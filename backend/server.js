const express = require('express')
const cors = require('cors')
const mysql = require('mysql2')
const router = express.Router()

const app = express();

app.use(express.json());
app.use(cors('*'));

const con = mysql.createConnection({
    host:"db",
    port:"3306",
    user:"root",
    password:"root",
    database:"test1"
})

app.get('/',(req,res)=>{
    const query = 'select * from students'
    con.query(query, (error, result) => {
        if(error)
        throw error.message;
        else
        res.send(result)
    })
})





app.post('/addStudent', (request, response) => {
    const { student_id,s_name, password, course ,passing_year, prn_no, dob } = request.body
    
    const query = 'insert into students ( student_id, s_name, password, course ,passing_year, prn_no, dob ) values(?,?,?,?)'
    con.query(query, [student_id,s_name, password, course ,passing_year, prn_no, dob,student_id], (error, result) => {
        response.send(result);
    })
})



app.put('/updateStudent/:student_id', (request, response) => {
    const { student_id } = request.params;
    const { s_name, password, course ,passing_year, prn_no, dob } = request.body
    
    const query = 'update students set s_name = ?, password = ?, course = ?, passing_year = ?,prn_no = ?,dob = ? where student_id = ?';

    con.query(query, [s_name, password, course ,passing_year, prn_no, dob,student_id], (error, result) => {
        response.send("Student updated successfully");
    })
})

app.delete("/deleteStudent/:student_id", (request, response) => {
    const { student_id } = request.params;
    const query =
      "delete from students where student_id = ?";
  
    con.query(query, [student_id], (error, result) => {
      response.send("Student Deleted Successfully");
    });
  });

app.listen(5000,"0.0.0.0",()=>{
    console.log("server started on port 5000")
})
